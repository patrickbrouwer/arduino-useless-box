#include <Servo.h>
Servo armServo;

const int  buttonPin = 2;
int switchstate = 0;

int counter = 1;
bool counted = false;

int const ServoStart = 180;
int const ServoEnd = 38;

void setup() {
  // put your setup code here, to run once:
  pinMode(buttonPin, INPUT);


  armServo.attach(3);
  armServo.write(ServoStart);
  delay(50);

  Serial.begin(9600);
}

void GoTostart() {
  armServo.write(ServoStart);
  delay(100);
}

void GoToEnd() {
  armServo.write(ServoEnd);
}

void loop() {
  // put your main code here, to run repeatedly:

  //if switch is pressed
  switchstate = digitalRead(2);
  //select 1 of the options
  delay(10);
  if (switchstate == LOW) {

    //count up
    if (counted == false) {

      counter = counter + 1;
      counted = true;

      Serial.print("counter = ");
      Serial.println(counter);
    }

    if (counter % 12 == 0) {
      WaitHalfWay();
      counter = counter + 1;
      

    }
    
    else if (counter % 16 == 0) {
      GoToCrazy();
      counter = counter + 1;
    }

    else if (counter % 21 == 0) {
      //GoToSlowEnd();
      counter = counter + 1;

    }

    else if (counter % 23 == 0) {
      //GoToSlowEnd();
      counter = counter + 1;
      //GoToSlowStart();
    }

    else if (counter % 27 == 0) {
      GoToLongWaitEnd();
      counter = counter + 1;
    }

    GoToEnd();
  } else {
    counted = false; //reset counted
    //go back to start
    GoTostart();
  

  }
  delay(50);



}



void GoToSlowEnd() {
  for (int position = ServoStart; position > ServoEnd ; position = position - 5) {
    armServo.write(position);
    delay(25);
  }
  armServo.write(ServoEnd);
  delay(25);
}

void GoToSlowStart() {
  for (int position = ServoEnd; position < ServoStart; position = position + 10) {
    armServo.write(position);
    delay(25);
  }
  delay(100);
}

void GoToCrazy() {
  for (int i = 0; i < 8; i = i + 1) {
    Serial.println(i);
    armServo.write(135);
    delay(60);
    armServo.write(120);
    delay(60);
  }
  armServo.write(140);
  delay(1000);

}

void GoToLongWaitEnd() {
  int waitTime = random(3000, 7000);

  delay(waitTime);
  armServo.write(ServoEnd);
  delay(100);
}

void WaitHalfWay() {
  int halfway = 90  ;

  armServo.write(halfway);
  delay(1000);

  armServo.write(ServoEnd);
  delay(100);

}





